import java.util.ArrayList;

public class Phonebook {
    //ArrayList
    private ArrayList<Contact> contacts;

    //Constructor

    //Default constructor
    public Phonebook(){
        contacts = new ArrayList<>();
    }

    //Parameterized constructor
    public Phonebook(ArrayList<Contact> contacts){
        this.contacts = contacts;
    }

    //Getter and Setter
    //Getter
    public ArrayList<Contact> getContacts(){
        return this.contacts;
    }

    //Setter
    public void setContacts(Contact contact) {
        contacts.add(contact);
    }


    public boolean isEmpty() {
        return contacts.isEmpty();
    }




}
