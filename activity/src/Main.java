import java.util.ArrayList;
public class Main {
    public static void main(String[] args) {

        Phonebook phonebook = new Phonebook();

        //Add contact1 = John Doe
        Contact contact1 = new Contact("John Doe", "+639152468596", "my home in Quezon City");
        //Add additional contact number/address as seen from the expected output
        contact1.setContactNumber("+639228547963");
        contact1.setAddress("my office in Makati City");

        //Add contact2 = Jane Doe
        Contact contact2 = new Contact("Jane Doe", "+639162148573", "my home in Caloocan City");
        //Add additional contact number/address as seen from the expected output
        contact2.setContactNumber("+639173698541");
        contact2.setAddress("my office in Pasay City");

        phonebook.setContacts(contact1);
        phonebook.setContacts(contact2);


        // Check if the phonebook is empty and display contact details
        if (phonebook.isEmpty()) {
            System.out.println("The phonebook is empty.");
        } else {
            for (Contact contact : phonebook.getContacts()) {
                System.out.println(contact.getName());
                System.out.println(contact.getName() + " has the following registered numbers:\n" + contact.getContactNumber());
                System.out.println(contact.getName() + " has the following registered addresses:\n" + contact.getAddress());
                System.out.println();
            }
        }




    }
}