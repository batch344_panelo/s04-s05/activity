public class Contact {

    //Properties
    private String name;
    private String contactNumbers;
    private String addresses;

    //Constructor
    //Default constructor
    public Contact(){}

    //Parameterized constructor
    public Contact(String name, String contactNumbers, String addresses){
        this.name = name;
        this.contactNumbers = contactNumbers;
        this.addresses = addresses;
    }

    //Getter and Setter
    //Getter
    public String getName(){
        return this.name;
    }

    public String getContactNumber(){
        return this.contactNumbers;
    }

    public String getAddress(){
        return this.addresses;
    }

    //Setter
    public void setName(String name){
        this.name = name;
    }

    //Had to adjust the setter methods in the code to match the format shown in the expected output image
    public void setContactNumber(String contactNumber) {
        if (contactNumber == null) {
            contactNumbers = contactNumber;
        } else {
            contactNumbers += "\n" + contactNumber;
        }
    }

    //Had to adjust the setter methods in the code to match the format shown in the expected output image
    public void setAddress(String address){
        if (address == null) {
            addresses = address;
        } else {
            addresses += "\n" + address;
        }
    }


}
